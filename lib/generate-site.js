'use strict'

const aggregateContent = require('@antora/content-aggregator')
const buildNavigation = require('@antora/navigation-builder')
const buildPlaybook = require('@antora/playbook-builder')
const classifyContent = require('@antora/content-classifier')
const convertDocuments = require('@antora/document-converter')
const createPageComposer = require('@antora/page-composer')
const loadUi = require('@antora/ui-loader')
const mapSite = require('@antora/site-mapper')
const produceRedirects = require('@antora/redirect-producer')
const publishSite = require('@antora/site-publisher')
const parseResourceId = require('@antora/content-classifier/lib/util/parse-resource-id')
const { resolveConfig: resolveAsciiDocConfig } = require('@antora/asciidoc-loader')
const path = require('path')

const UNRESOLVED_XREF_RX = /<a href="#">([^>]+?\.adoc)(#[^<]+?)?(?:\|([^<]+))?<\/a>/g
const UNRESOLVED_XREF_HINT = 'href="#"'

async function generateSite (args, env) {
  const playbook = buildPlaybook(args, env)
  const [contentCatalog, uiCatalog] = await Promise.all([
    aggregateContent(playbook).then((contentAggregate) => classifyContent(playbook, contentAggregate)),
    loadUi(playbook),
  ])
  const asciidocConfig = resolveAsciiDocConfig(playbook)
  const primarySiteUrl = asciidocConfig.attributes['primary-site-url']
  const pages = convertDocuments(contentCatalog, asciidocConfig)
  // QUESTION should supplemental-sitemap.json also be an AsciiDoc attribute? or perhaps allow an override
  if (primarySiteUrl) divertUnresolvedXrefs(pages, path.join(playbook.dir, 'supplemental-sitemap.json'), primarySiteUrl)
  const navigationCatalog = buildNavigation(contentCatalog, asciidocConfig)
  const composePage = createPageComposer(playbook, contentCatalog, uiCatalog, env)
  pages.forEach((page) => composePage(page, contentCatalog, navigationCatalog))
  const siteFiles = mapSite(playbook, pages).concat(produceRedirects(playbook, contentCatalog))
  siteFiles.push(createSupplementalSitemap(contentCatalog.getComponents(), pages))
  if (playbook.site.url) siteFiles.push(composePage(create404Page()))
  const siteCatalog = { getFiles: () => siteFiles }
  return publishSite(playbook, [contentCatalog, uiCatalog, siteCatalog])
}

function create404Page () {
  return {
    title: 'Page Not Found',
    mediaType: 'text/html',
    src: { stem: '404' },
    out: { path: '404.html' },
    pub: { url: '/404.html', rootPath: '' },
  }
}

function createSupplementalSitemap(components, pages) {
  const data = {
    components: components.map(({ name, latest: { version: latest } }) => ({ name, latest })),
    pages: pages.map(({ src, asciidoc, pub }) => ({ id: generatePageId(src), url: pub.url, title: asciidoc.doctitle })),
  }
  return {
    contents: Buffer.from(JSON.stringify(data, null, 2)),
    mediaType: 'application/json',
    out: { path: 'supplemental-sitemap.json' },
  }
}

function generatePageId (src) {
  return `${src.version}@${src.component}:${src.module}:${src.relative}`
}

function divertUnresolvedXrefs (pages, supplementalSitemapFile, primarySiteUrl) {
  let data
  try {
    // TODO download supplemental sitemap if file is a URL
    data = require(supplementalSitemapFile)
  } catch (e) {
    console.warn(`antora: WARNING: could not locate supplemental sitemap file; unresolved xrefs will not be processed`)
    return
  }
  const versionsByComponent = data.components.reduce((accum, { name, latest }) => (accum[name] = latest) && accum, {})
  const pagesById = data.pages.reduce((accum, { id, url, title }) => (accum[id] = { url, title }) && accum, {})
  pages.forEach((page) => {
    if (!page.contents.includes(UNRESOLVED_XREF_HINT)) return
    let modified = false
    const contents = page.contents.toString().replace(UNRESOLVED_XREF_RX, (match, targetId, hash, text) => {
      const targetSrc = parseResourceId(targetId, page.src)
      if (!targetSrc.version) targetSrc.version = versionsByComponent[targetSrc.component]
      const targetPage = pagesById[generatePageId(targetSrc)]
      if (targetPage) {
        modified = true
        return `<a href="${primarySiteUrl}${targetPage.url}${hash || ''}" target="_blank">${text || targetPage.title}</a>`
      } else {
        console.warn(`antora: WARNING: could not resolve page ID of xref in primary site: ${generatePageId(targetSrc)}`)
        return match
      }
    })
    if (modified) page.contents = Buffer.from(contents)
  })
}

module.exports = generateSite
